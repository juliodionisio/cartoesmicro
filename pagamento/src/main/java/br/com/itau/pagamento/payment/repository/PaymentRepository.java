package br.com.itau.pagamento.payment.repository;


import br.com.itau.pagamento.payment.models.Payment;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PaymentRepository extends CrudRepository<Payment, Long> {

    List<Payment> findAllByCreditCardID(Long creditCardId);
}
