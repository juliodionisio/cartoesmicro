package br.com.itau.pagamento.payment.models;


import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table
public class Payment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String description;

    @Column
    private BigDecimal value;

    @Column
    private Long creditCardID;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public Long getCreditCardID() {
        return creditCardID;
    }

    public void setCreditCardID(Long creditCardID) {
        this.creditCardID = creditCardID;
    }
}
