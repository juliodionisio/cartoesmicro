package br.com.itau.pagamento.payment.clients;


import br.com.itau.pagamento.payment.models.CreditCard;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "cartao")
public interface CreditCardClient {

    @GetMapping("/cartao")
    CreditCard getById(@RequestParam(name = "id") Long id);
}
