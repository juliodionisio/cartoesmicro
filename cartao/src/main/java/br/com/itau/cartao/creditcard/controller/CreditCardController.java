package br.com.itau.cartao.creditcard.controller;

import br.com.itau.cartao.creditcard.model.CreditCard;
import br.com.itau.cartao.creditcard.model.dto.*;
import br.com.itau.cartao.creditcard.service.CreditCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CreditCardController {

    @Autowired
    private CreditCardService creditCardService;

    @Autowired
    private CreditCardMapper mapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CreateCreditCardResponse create(@Valid @RequestBody CreateCreditCardRequest createCreditCardRequest) {
        CreditCard creditCard = mapper.toCreditCard(createCreditCardRequest);

        creditCard = creditCardService.create(creditCard);

        return mapper.toCreateCreditCardResponse(creditCard);
    }

    @PatchMapping("/{number}")
    public UpdateCreditCardResponse update(@PathVariable String number, @RequestBody UpdateCreditCardRequest updateCreditCardRequest) {
        updateCreditCardRequest.setNumber(number);
        CreditCard creditCard = mapper.toCreditCard(updateCreditCardRequest);

        creditCard = creditCardService.update(creditCard);

        return mapper.toUpdateCreditCardResponse(creditCard);
    }

    @GetMapping
    public GetCreditCardResponse getByNumber(@RequestParam(name = "number", required = false) String number, @RequestParam(name = "id", required = false) Long id) {
        CreditCard creditCard = new CreditCard();
        if(number != null){
            creditCard = creditCardService.getByNumber(number);
        } else {
            creditCard = creditCardService.getById(id);
        }
        return mapper.toGetCreditCardResponse(creditCard);
    }


}
