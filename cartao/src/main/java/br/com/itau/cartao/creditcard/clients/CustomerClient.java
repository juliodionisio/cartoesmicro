package br.com.itau.cartao.creditcard.clients;


import br.com.itau.cartao.creditcard.model.Customer;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cliente")
public interface CustomerClient {

    @GetMapping("/cliente/{id}")
    Customer getCustomerByID(@PathVariable Long id);

}
